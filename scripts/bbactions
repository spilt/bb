#!/bin/sh
# This file defines file actions for bb. Each command will be run with files in
# the $@ arguments.
#
# The format is: ## Action \n { action }

## Edit file(s)
{
    $EDITOR "$@"
}

## Delete file(s)
{
    printf "\033[1mDeleting the following:\n\033[33m$(printf '  %s\n' "$@")\033[0m" | bbunscroll | more
    bbconfirm
    rm -rf "$@"
    bbcmd deselect refresh
}

## Page through file(s)
{
    less -XK "$@"
}

## Rename file(s)
{
    for f; do
        newname="$(bbask "Rename $(printf "\033[33m%s\033[39m" "${f##*/}"): " "${f##*/}")" || break
        r="$(dirname "$f")/$newname"
        [ "$r" = "$f" ] && continue
        [ -e "$r" ] && printf "\033[31;1m$r already exists! It will be overwritten.\033[0m "
            && bbconfirm && { rm -rf "$r" || { bbpause; exit; }; }
        mv "$f" "$r" || { bbpause; exit; }
        bbcmd deselect:"$f" select:"$r"
        [ "$f" = "$BB" ] && bbcmd goto:"$r"
    done
    bbcmd refresh
}

## Rename file(s) with regex
{
    if ! command -v rename >/dev/null; then
        printf '\033[31;1mThe `rename` command is not installed. Please install it to use this key binding.\033[0m\n'
        bbpause; exit 1
    fi
    patt="$(bbask "Replace pattern: ")"
    rep="$(bbask "Replacement: ")"
    printf "\033[1mRenaming:\n\033[33m$(rename -nv "$patt" "$rep" "$@")\033[0m" | bbunscroll | more
    bbconfirm
    rename -i "$patt" "$rep" "$@"
    bbcmd deselect refresh
}

## Pass file(s) as arguments to a command
{
    cmd="$(bbask '@')"
    sh -c "$cmd \"\$@\"" -- "$@" || true
    bbpause
    bbcmd refresh
}

## Pipe file(s) to a command
{
    cmd="$(bbask '|')"
    printf '%s\n' "$@" | sh -c "$cmd" || true
    bbpause
    bbcmd refresh
}


## Run file(s)
{
    for f; do "$f"; done
    bbpause
}

## Open file(s)
{
    if [ "$(uname)" = "Darwin" ]; then
        for f; do open "$f"; done
    else
        xdg-open "$f"
    fi
}

## Print filename(s)
{
    echo "$@"
    bbpause
}
